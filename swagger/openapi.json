{
  "openapi": "3.0.3",
  "info": {
    "title": "Swagger Chat Server - Rest API",
    "description": "Documentation",
    "version": "0.0.1"
  },
  "tags": [
    {
      "name": "users",
      "description": "Login, SignUp and more methods of getting information about profile"
    },
    {
      "name": "friends",
      "description": "Friend requests and more methods of managing user's contacts"
    }
  ],
  "paths": {
    "/login": {
      "post": {
        "tags": [
          "users"
        ],
        "summary": "Sign in to user profile",
        "description": "Sing in and get autorization token",
        "operationId": "login",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "required": [
                  "login",
                  "password"
                ],
                "type": "object",
                "properties": {
                  "login": {
                    "type": "string",
                    "example": "user"
                  },
                  "password": {
                    "type": "string",
                    "example": "12345!Aa"
                  }
                }
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "id": {
                      "type": "integer",
                      "format": "int64",
                      "example": 10
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Invalid credentials"
          },
          "500": {
            "description": "Internal error"
          }
        }
      }
    },
    "/signup": {
      "post": {
        "tags": [
          "users"
        ],
        "summary": "User registration",
        "description": "Signup and get token autorization",
        "operationId": "signUp",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "required": [
                  "login",
                  "password",
                  "email"
                ],
                "type": "object",
                "properties": {
                  "login": {
                    "type": "string",
                    "example": "user"
                  },
                  "password": {
                    "type": "string",
                    "example": "12345!Aa"
                  },
                  "email": {
                    "type": "string",
                    "example": "user@gmail.com"
                  },
                  "publicKey": {
                    "type": "string",
                    "example": "KJHihga763hkgKUDGSaW32"
                  }
                }
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "id": {
                      "type": "integer",
                      "format": "int64",
                      "example": 10
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Invalid credentials / Incorrect publicKey / Incorrect login / Incorrect password / Incorrect email address"
          },
          "500": {
            "description": "Internal error"
          }
        }
      }
    },
    "/search-for-login": {
      "get": {
        "tags": [
          "friends"
        ],
        "summary": "Find users by login",
        "description": "Multiple users whose login matches the one you are looking for",
        "operationId": "searchForLogin",
        "parameters": [
          {
            "name": "login",
            "in": "query",
            "description": "Login of the user you want to find",
            "required": false,
            "explode": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "limit",
            "in": "query",
            "description": "Max amount of matched users",
            "required": false,
            "explode": true,
            "schema": {
              "type": "integer",
              "default": 10
            }
          },
          {
            "name": "offset",
            "in": "query",
            "required": false,
            "explode": true,
            "schema": {
              "type": "integer",
              "default": 0
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "integer",
                        "format": "int64",
                        "example": 10
                      },
                      "login": {
                        "type": "string",
                        "example": "login"
                      }
                    }
                  }
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "500": {
            "description": "Internal error"
          }
        }
      }
    },
    "/friends-list": {
      "get": {
        "tags": [
          "friends"
        ],
        "summary": "Get list of user's friends",
        "description": "Can get friend's id by login or all ids and logins",
        "operationId": "friendsList",
        "parameters": [
          {
            "name": "login",
            "in": "query",
            "description": "Login of the user you want to find",
            "required": false,
            "explode": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "limit",
            "in": "query",
            "description": "Max amount of matched users",
            "required": false,
            "explode": true,
            "schema": {
              "type": "integer",
              "default": 10
            }
          },
          {
            "name": "offset",
            "in": "query",
            "required": false,
            "explode": true,
            "schema": {
              "type": "integer",
              "default": 0
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "integer",
                        "format": "int64",
                        "example": 10
                      },
                      "login": {
                        "type": "string",
                        "example": "login"
                      }
                    }
                  }
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "500": {
            "description": "Internal error"
          }
        }
      }
    },
    "/friend": {
      "get": {
        "tags": [
          "friends"
        ],
        "summary": "Get information about friend",
        "description": "Get information about friend",
        "operationId": "getFriend",
        "parameters": [
          {
            "name": "id",
            "in": "query",
            "description": "Friend's id",
            "required": false,
            "explode": true,
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "id": {
                      "type": "integer",
                      "format": "int64",
                      "example": 10
                    },
                    "login": {
                      "type": "string",
                      "example": "login"
                    },
                    "lastBeen": {
                      "type": "integer",
                      "format": "int64",
                      "example": 1691920913231
                    },
                    "online": {
                      "type": "string",
                      "example": "online"
                    },
                    "publicKey": {
                      "type": "string",
                      "example": "1uiga873sdKJHkw832kdsjx"
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Invalid friend id / This user is not your friend"
          },
          "401": {
            "description": "Unauthorized"
          },
          "500": {
            "description": "Internal error"
          }
        }
      }
    },
    "/friend-request": {
      "post": {
        "tags": [
          "friends"
        ],
        "summary": "Add new friend",
        "description": "Send or confirm friend request",
        "operationId": "friendRequest",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "required": [
                  "id"
                ],
                "type": "object",
                "properties": {
                  "id": {
                    "type": "integer",
                    "format": "int64",
                    "example": 10
                  }
                }
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Success"
          },
          "400": {
            "description": "Bad request / Already requested / User not found"
          },
          "401": {
            "description": "Unauthorized"
          },
          "500": {
            "description": "Internal error"
          }
        }
      }
    },
    "/unfriend": {
      "post": {
        "tags": [
          "friends"
        ],
        "summary": "Remove from friends",
        "description": "The other user still has friend request information",
        "operationId": "unfriend",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "required": [
                  "id"
                ],
                "type": "object",
                "properties": {
                  "id": {
                    "type": "integer",
                    "format": "int64",
                    "example": 10
                  }
                }
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Success"
          },
          "400": {
            "description": "Bad request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "500": {
            "description": "Internal error"
          }
        }
      }
    },
    "/direct-chat": {
      "get": {
        "tags": [
          "direct"
        ],
        "summary": "Get messages of direct chat with user",
        "description": "Get messages of direct chat with user",
        "operationId": "directChat",
        "parameters": [
          {
            "name": "id",
            "in": "query",
            "description": "Friend's id",
            "required": false,
            "explode": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "limit",
            "in": "query",
            "description": "Max amount of messages",
            "required": false,
            "explode": true,
            "schema": {
              "type": "integer",
              "default": 10
            }
          },
          {
            "name": "offset",
            "in": "query",
            "required": false,
            "explode": true,
            "schema": {
              "type": "integer",
              "default": 0
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "fromID": {
                        "type": "integer",
                        "format": "int64",
                        "example": 10
                      },
                      "fromLogin": {
                        "type": "string",
                        "example": "login"
                      },
                      "messageID": {
                        "type": "integer",
                        "format": "int64",
                        "example": 10
                      },
                      "message": {
                        "type": "string",
                        "example": "Hello!"
                      },
                      "time": {
                        "type": "integer",
                        "format": "int64",
                        "example": 1692354552099
                      }
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "500": {
            "description": "Internal error"
          }
        }
      }
    }
  }
}