package main

import (
	"context"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/lestrrat-go/jwx/jwt"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/jwtauth"
	"github.com/gorilla/websocket"
	"github.com/joho/godotenv"

	"chat/server/app/libs/db"
	"chat/server/app/libs/helpers"
	"chat/server/app/libs/redis"
	"chat/server/app/routes-handlers"
	wsConnection "chat/server/app/websocket"
)

var tokenAuth *jwtauth.JWTAuth

func main() {
	err := godotenv.Load()
	if err != nil {
		panic("Error loading .env file")
	}

	db.CreatePool()
	redis.CreateClient()
	wsConnection.NewHub()
	onStarted()

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	r.Use(cors.Handler(cors.Options{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"https://*", "http://*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH"},
		AllowedHeaders:   []string{"Origin", "Authorization", "Content-Type", "Accept-Encoding"},
		ExposedHeaders:   []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Credentials", "Access-Control-Allow-Headers", "Access-Control-Allow-Methods"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))

	jwtSecret := helpers.GetEnv("JWT_SECRET", "")
	tokenAuth = jwtauth.New("HS256", []byte(jwtSecret), nil)

	// method without token
	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(tokenAuth))
		r.Use(UnloggedInRedirector)

		r.Get("/search-for-login", handlers.SearchForLogin)
		r.Get("/friends-list", handlers.FriendsList)
		r.Get("/direct-chat", handlers.DirectChat)
		//r.Get("/room-chat", handlers.RoomChat)
		r.Get("/friend", handlers.GetFriend)

		r.Post("/friend-request", handlers.FriendRequest)
		r.Post("/unfriend", handlers.Unfriend)
		r.Post("/me", handlers.Me)
	})

	// methods which use token
	r.Group(func(r chi.Router) {
		r.Post("/login", handlers.Login)
		r.Post("/signup", handlers.SignUp)
	})

	// websoket
	r.Get("/ws", func(w http.ResponseWriter, r *http.Request) {
		tokenStr := jwtauth.TokenFromQuery(r)
		token, err := jwtauth.VerifyToken(tokenAuth, tokenStr)

		if err != nil {
			http.Redirect(w, r, "/login", http.StatusUnauthorized)
			return
		}

		id, ok := token.Get("id")
		if !ok {
			http.Redirect(w, r, "/login", http.StatusUnauthorized)
			return
		}

		userID, ok := id.(float64)
		if !ok {
			http.Redirect(w, r, "/login", http.StatusUnauthorized)
			return
		}

		var upgrader = websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		}

		// Upgrading the HTTP connection socket connection
		connection, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println(err)
			return
		}

		wsConnection.CreateSocket(connection, int(userID))
	})

	port := helpers.GetEnv("PORT", "3000")
	http.ListenAndServe(":"+port, r)
}

func UnloggedInRedirector(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		token, _, err := jwtauth.FromContext(r.Context())

		if token == nil || jwt.Validate(token) != nil {
			log.Println(err)
			http.Redirect(w, r, "/login", http.StatusUnauthorized)
		} else {
			rtch, _ := token.Get("rtch")
			refresh, _ := token.Get("refresh")
			id, _ := token.Get("id")

			ctx := context.Background()
			val, _ := redis.Client.Get(ctx, "refreshToken:"+strconv.FormatInt(int64(id.(float64)), 10)).Result()

			if val == refresh {
				if int64(rtch.(float64)) < time.Now().UTC().Unix() {
					// time to update token
					
					refresh := helpers.RandStr()
					claims := map[string]interface{}{
						"id":      id,
						"exp":     time.Now().Add(7 * 24 * time.Hour),
						"refresh": refresh,
						"rtch":    time.Now().Add(4 * 24 * time.Hour).UTC().Unix(), // refresh token check 
					}
				
					ctx := context.Background()
					_, errRedis := redis.Client.Set(ctx, "refreshToken:"+strconv.FormatInt(int64(id.(float64)), 10), refresh, 7*24*time.Hour).Result()
				
					if errRedis != nil {
						http.Redirect(w, r, "/login", http.StatusUnauthorized)
						return
					}
				
					token := helpers.CreateToken(claims)
				
					http.SetCookie(w, &http.Cookie{
						Expires: time.Now().Add(7 * 24 * time.Hour),
						//SameSite: http.SameSiteLaxMode,
						// Uncomment below for HTTPS:
						// Secure: true,
						Name:  "jwt", // Must be named "jwt" or else the token cannot be searched for by jwtauth.Verifier.
						Value: token,
					})
				}

				next.ServeHTTP(w, r)
				return
			}
		
			http.Redirect(w, r, "/login", http.StatusUnauthorized)
		}

	})
}

func onStarted() {
	queryString := "UPDATE users SET online = 'offline'"
	_, err := db.Connection.Exec(queryString)

	if err != nil {
		log.Panicln("Error in onStarted", err.Error())
	}
}
