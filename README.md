# chat-go-server

## Documentation

```
$ cd swagger
$ docker pull swaggerapi/swagger-ui
$ docker run -d -p 8081:8080 -e SWAGGER_JSON=/mnt/openapi.json -v ${PWD}:/mnt swaggerapi/swagger-ui
```

Open http://localhost:8081 to watch the documentation of REST API