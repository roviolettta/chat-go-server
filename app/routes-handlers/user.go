package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/mail"
	"regexp"
	"strconv"
	"time"

	"github.com/go-chi/jwtauth"

	"chat/server/app/libs/db"
	"chat/server/app/libs/helpers"
	"chat/server/app/libs/redis"
)

func Login(w http.ResponseWriter, r *http.Request) {

	var bodyBytes []byte
	if r.Body != nil {
		bodyBytes, _ = io.ReadAll(r.Body)
	}

	var bodyJson User
	err := json.Unmarshal(bodyBytes, &bodyJson)
	if err != nil {
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "Incorrect credentials",
		})
		return
	}

	fmt.Print("Login ", bodyJson.Login, " ", bodyJson.Password, "\n")

	res, err := db.Connection.Query("SELECT id, login, password, email FROM users WHERE login = ? && password = ?", bodyJson.Login, bodyJson.Password)
	if err != nil {
		fmt.Print(err, "\n")

		WriteResponse(w, http.StatusInternalServerError, map[string]string{
			"message": "Internal error",
		})
		return
	}

	defer res.Close()

	if res.Next() {

		var user User
		err := res.Scan(&user.Id, &user.Login, &user.Password, &user.Email)

		if err != nil {
			WriteResponse(w, http.StatusInternalServerError, map[string]string{
				"message": err.Error(),
			})
			return
		}

		refresh := helpers.RandStr()

		claims := map[string]interface{}{
			"id":      user.Id,
			"exp":     time.Now().Add(7 * 24 * time.Hour), // period for a week
			"refresh": refresh,
			"rtch":    time.Now().Add(4 * 24 * time.Hour).UTC().Unix(), // refresh token check
		}

		token := helpers.CreateToken(claims)

		ctx := context.Background()
		_, errRedis := redis.Client.Set(ctx, "refreshToken:"+strconv.Itoa(user.Id), refresh, 7*24*time.Hour).Result()

		if errRedis != nil {
			WriteResponse(w, http.StatusInternalServerError, map[string]string{
				"message": errRedis.Error(),
			})
			return
		}

		http.SetCookie(w, &http.Cookie{
			Expires: time.Now().Add(7 * 24 * time.Hour),
			//SameSite: http.SameSiteLaxMode,
			// Uncomment below for HTTPS:
			// Secure: true,
			Name:  "jwt", // Must be named "jwt" or else the token cannot be searched for by jwtauth.Verifier.
			Value: token,
		})

		WriteResponse(w, http.StatusOK, map[string]interface{}{
			"id":    user.Id,
			"login": user.Login,
			"email": user.Email,
		})
	} else {
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "Incorrect credentials",
		})
	}
}

func SignUp(w http.ResponseWriter, r *http.Request) {
	var bodyBytes []byte
	if r.Body != nil {
		bodyBytes, _ = io.ReadAll(r.Body)
	}

	var bodyJson User
	err := json.Unmarshal(bodyBytes, &bodyJson)
	if err != nil {
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "Incorrect credentials",
		})
		return
	}

	if len(bodyJson.PublicKey) == 0 {
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "Incorrect publicKey",
		})
		return
	}

	loginTest, _ := regexp.MatchString("^[a-z0-9]([._]??[a-z0-9]){2,15}$", bodyJson.Login)
	if !loginTest {
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "Incorrect login",
		})
		return
	}

	testsForPass := []string{".{7,64}", "[a-z]", "[A-Z]", "[0-9]", `[!@#$%^&]`}
	for _, test := range testsForPass {
		matched, _ := regexp.MatchString(test, bodyJson.Password)
		if !matched {
			WriteResponse(w, http.StatusBadRequest, map[string]string{
				"message": "Incorrect password",
			})
			return
		}
	}

	_, parseEmail := mail.ParseAddress(bodyJson.Email)
	if parseEmail != nil {
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "Incorrect email address",
		})
		return
	}

	queryString := "INSERT INTO users(login, email, password, lastBeen, publicKey) VALUES (?, ?, ?, ?, ?)"
	res, err := db.Connection.Exec(queryString, bodyJson.Login, bodyJson.Email, bodyJson.Password, time.Now().UTC().UnixMilli(), bodyJson.PublicKey)

	if err != nil {
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": err.Error(),
		})
		return
	}

	id, err := res.LastInsertId()
	if err != nil {
		log.Printf("Error happened in LastInsertId. Err: %s", err.Error())
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "Internal error",
		})
		return
	}

	refresh := helpers.RandStr()

	claims := map[string]interface{}{
		"id":      id,
		"exp":     time.Now().Add(7 * 24 * time.Hour),
		"refresh": refresh,
		"rtch":    time.Now().Add(4 * 24 * time.Hour).UTC().Unix(), // refresh token check
	}

	ctx := context.Background()
	_, errRedis := redis.Client.Set(ctx, "refreshToken:"+strconv.FormatInt(id, 10), refresh, 7*24*time.Hour).Result()

	if errRedis != nil {
		WriteResponse(w, http.StatusInternalServerError, map[string]string{
			"message": errRedis.Error(),
		})
		return
	}

	token := helpers.CreateToken(claims)

	http.SetCookie(w, &http.Cookie{
		Expires: time.Now().Add(7 * 24 * time.Hour),
		//SameSite: http.SameSiteLaxMode,
		// Uncomment below for HTTPS:
		// Secure: true,
		Name:  "jwt", // Must be named "jwt" or else the token cannot be searched for by jwtauth.Verifier.
		Value: token,
	})

	WriteResponse(w, http.StatusOK, map[string]interface{}{
		"id":    id,
		"login": bodyJson.Login,
		"email": bodyJson.Email,
	})
}

func Me(w http.ResponseWriter, r *http.Request) {

	_, claims, _ := jwtauth.FromContext(r.Context())
	fmt.Print("id = ", claims["id"], "\n")
}
