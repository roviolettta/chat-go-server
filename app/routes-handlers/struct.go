package handlers

type User struct {
	Id        int
	Login     string
	Password  string
	Email     string
	PublicKey string
	Online    string
	LastBeen  int
}
