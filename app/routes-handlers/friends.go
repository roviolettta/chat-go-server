package handlers

import (
	"chat/server/app/libs/db"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"database/sql"

	wsConnection "chat/server/app/websocket"
	"github.com/go-chi/jwtauth"
)

func SearchForLogin(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()

	login := ""
	limit := "10"
	offset := "0"

	if len(query["login"]) > 0 {
		login = query["login"][0]
	}

	if len(query["limit"]) > 0 {
		limit = query["limit"][0]
	}

	if len(query["offset"]) > 0 {
		offset = query["offset"][0]
	}

	rows, err := db.Connection.Query("SELECT id, login FROM users WHERE login LIKE concat('%', ?, '%') LIMIT ? OFFSET ?", login, limit, offset)
	if err != nil {
		log.Print(err, "\n")

		WriteResponse(w, http.StatusInternalServerError, map[string]string{
			"message": "Internal error",
		})
		return
	}

	defer rows.Close()

	users := make([]map[string]interface{}, 0)

	// Loop through rows, using Scan to assign column data to struct fields.
	for rows.Next() {
		var id int
		var login string

		err := rows.Scan(&id, &login)

		if err != nil {
			log.Println(err.Error())
			WriteResponse(w, http.StatusInternalServerError, map[string]string{
				"message": "Internal error",
			})
			return
		}

		u := map[string]interface{}{
			"id":    id,
			"login": login,
		}

		users = append(users, u)
	}

	WriteResponse(w, http.StatusOK, users)
}

func FriendRequest(w http.ResponseWriter, r *http.Request) {
	// get input data
	_, claims, _ := jwtauth.FromContext(r.Context())
	fromUserID := int(claims["id"].(float64))

	var bodyBytes []byte
	if r.Body != nil {
		bodyBytes, _ = io.ReadAll(r.Body)
	}

	var bodyJson User
	err := json.Unmarshal(bodyBytes, &bodyJson)
	if err != nil {
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "Bad request",
		})
		return
	}

	toUserID := bodyJson.Id

	// can't send user request to myself
	if fromUserID == toUserID {
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "Can't send user request to myself",
		})
		return
	}

	// create db transaction
	tx, _ := db.Connection.Begin()

	// maybe 'friend request' was already requested
	queryString := "SELECT * FROM friends WHERE user_id = ? && friend_id = ?"
	row := tx.QueryRow(queryString, fromUserID, toUserID)

	var (
		user_id   int
		friend_id int
	)

	err = row.Scan(&user_id, &friend_id)
	if err == nil && user_id != 0 && friend_id != 0 {
		tx.Rollback()
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "Already requested",
		})
		return
	}

	// try to save 'friend request'
	queryString = "INSERT INTO friends(user_id, friend_id) VALUES (?, ?)"
	_, errDB := tx.Exec(queryString, fromUserID, toUserID)
	if errDB != nil {
		tx.Rollback()

		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "User not found",
		})
		return
	}

	// maybe another user already sent request to this user
	queryStringCross := "SELECT * FROM friends WHERE user_id = ? && friend_id = ?"
	rowCross := tx.QueryRow(queryStringCross, toUserID, fromUserID)

	var (
		friend_id_1 int
		friend_id_2 int
	)

	err = rowCross.Scan(&friend_id_1, &friend_id_2)
	if err == nil && friend_id_1 != 0 && friend_id_2 != 0 {
		tx.Commit()
		var socketEvent1, socketEvent2 wsConnection.SocketEvent

		// event of confirm friend request
		socketEvent1.Event = "friend request confirmed"
		socketEvent1.Payload = map[string]interface{}{"from": fromUserID}
		wsConnection.EmitToSpecificClient(socketEvent1, toUserID)

		socketEvent2.Event = "friend request confirmed"
		socketEvent2.Payload = map[string]interface{}{"from": toUserID}
		wsConnection.EmitToSpecificClient(socketEvent2, fromUserID)

		WriteResponse(w, http.StatusOK, map[string]string{
			"message": "Success",
		})

		return
	}

	tx.Commit()

	var socketEvent wsConnection.SocketEvent

	// event of new friend request
	socketEvent.Event = "friend request"
	socketEvent.Payload = map[string]interface{}{
		"from": fromUserID,
	}

	wsConnection.EmitToSpecificClient(socketEvent, toUserID)

	WriteResponse(w, http.StatusOK, map[string]string{
		"message": "Success",
	})
}

func FriendsList(w http.ResponseWriter, r *http.Request) {
	// get input data
	_, claims, _ := jwtauth.FromContext(r.Context())
	fromUserID := int(claims["id"].(float64))

	query := r.URL.Query()

	login := ""
	limit := "10"
	offset := "0"

	if len(query["login"]) > 0 {
		login = query["login"][0]
	}

	if len(query["limit"]) > 0 {
		limit = query["limit"][0]
	}

	if len(query["offset"]) > 0 {
		offset = query["offset"][0]
	}

	queryString := `SELECT id, login
					FROM 
						users
							RIGHT JOIN
						(SELECT T.user_id FROM ( 
							SELECT user_id, friend_id FROM friends 
							WHERE user_id IN ( 
								SELECT friend_id FROM friends 
								WHERE user_id = ? 
							) 
						) AS T 
						WHERE T.friend_id = ?) AS T2 ON users.id = T2.user_id
					WHERE login LIKE concat('%', ?, '%')
					LIMIT ? OFFSET ?`

	rows, err := db.Connection.Query(queryString, fromUserID, fromUserID, login, limit, offset)
	if err != nil {
		log.Println(err.Error())
		WriteResponse(w, http.StatusInternalServerError, map[string]string{
			"message": "Internal error",
		})
		return
	}

	defer rows.Close()

	friends := make([]map[string]interface{}, 0)

	for rows.Next() {

		var id int
		var login string
		err := rows.Scan(&id, &login)

		if err != nil {
			WriteResponse(w, http.StatusInternalServerError, map[string]string{
				"message": err.Error(),
			})
			return
		}

		u := map[string]interface{}{
			"id":    id,
			"login": login,
		}

		friends = append(friends, u)
	}

	WriteResponse(w, http.StatusOK, friends)
}

func Unfriend(w http.ResponseWriter, r *http.Request) {

	// get input data
	_, claims, _ := jwtauth.FromContext(r.Context())
	userID := int(claims["id"].(float64))

	var bodyBytes []byte
	if r.Body != nil {
		bodyBytes, _ = io.ReadAll(r.Body)
	}

	var bodyJson User
	err := json.Unmarshal(bodyBytes, &bodyJson)
	if err != nil {
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "Bad request",
		})
		return
	}

	friendID := bodyJson.Id

	queryString := "DELETE FROM friends WHERE user_id = ? && friend_id = ?"
	_, errDB := db.Connection.Exec(queryString, userID, friendID)

	if errDB != nil {
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": errDB.Error(),
		})
		return
	}

	WriteResponse(w, http.StatusOK, map[string]string{
		"message": "Success",
	})
}

func GetFriend(w http.ResponseWriter, r *http.Request) {
	// get input data
	_, claims, _ := jwtauth.FromContext(r.Context())
	userID := int(claims["id"].(float64))

	query := r.URL.Query()

	var friendID string

	if len(query["id"]) > 0 {
		friendID = query["id"][0]
	} else {
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "Invalid friend id",
		})
		return
	}

	var (
		user_id   int
		friend_id int
	)

	// create db transaction
	tx, _ := db.Connection.Begin()

	queryString := "SELECT * FROM friends WHERE user_id = ? && friend_id = ?"
	row := tx.QueryRow(queryString, userID, friendID) // userID -> friendID

	err := row.Scan(&user_id, &friend_id)
	if err != nil {
		tx.Rollback()
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "This user is not your friend",
		})
		return
	}

	queryString = "SELECT * FROM friends WHERE user_id = ? && friend_id = ?"
	row2 := tx.QueryRow(queryString, friendID, userID) // userID <- friendID

	err2 := row2.Scan(&user_id, &friend_id)
	if err2 != nil {
		tx.Rollback()
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "This user is not your friend",
		})
		return
	}

	queryString = "SELECT id, login, publicKey, online, lastBeen FROM users WHERE id = ?"
	row3 := tx.QueryRow(queryString, friendID)

	var user User
	var publicKey sql.NullString

	err3 := row3.Scan(&user.Id, &user.Login, &publicKey, &user.Online, &user.LastBeen)
	if err3 != nil {
		tx.Rollback()
		WriteResponse(w, http.StatusBadRequest, map[string]string{
			"message": "Internal error",
		})
		return
	}

	WriteResponse(w, http.StatusOK, map[string]interface{}{
		"id":        user.Id,
		"login":     user.Login,
		"publicKey": publicKey.String,
		"online":    user.Online,
		"lastBeen":  user.LastBeen,
	})
}
