package handlers

import (
	"log"
	"net/http"
	"encoding/json"
)

func WriteResponse(w http.ResponseWriter, status int, resp any) {
	w.Header().Set("Content-Type", "application/json")

	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.Printf("Error happened in JSON marshal. Err: %s", err)
	}

	w.WriteHeader(status)
	w.Write(jsonResp)
}