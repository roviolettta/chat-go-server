package handlers

import (
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/jwtauth"

	"chat/server/app/libs/db"
)

func DirectChat(w http.ResponseWriter, r *http.Request) {
	// get input data
	_, claims, _ := jwtauth.FromContext(r.Context())
	userID := int(claims["id"].(float64))

	query := r.URL.Query()

	friendID := 0
	limit := "10"
	offset := "0"

	if len(query["id"]) > 0 {
		i, err := strconv.Atoi(query["id"][0])
		if err != nil {
			WriteResponse(w, http.StatusBadRequest, map[string]string{
				"message": "Bad request",
			})

			return
		}

		friendID = i
	}

	if len(query["limit"]) > 0 {
		limit = query["limit"][0]
	}

	if len(query["offset"]) > 0 {
		offset = query["offset"][0]
	}

	queryString := `SELECT T.id, T.from_user_id, login, T.message, T.time
									FROM 
									users
										RIGHT JOIN
									(SELECT id, from_user_id, message, time FROM direct_messages 
									WHERE from_user_id = ? && to_user_id = ? OR from_user_id = ? && to_user_id = ?
									ORDER BY time DESC
									LIMIT ? OFFSET ?) AS T ON users.id = T.from_user_id`

	rows, err := db.Connection.Query(queryString, userID, friendID, friendID, userID, limit, offset)
	if err != nil {
		log.Print(err, "\n")

		WriteResponse(w, http.StatusInternalServerError, map[string]string{
			"message": "Internal error",
		})
		return
	}

	defer rows.Close()

	messages := make([]map[string]interface{}, 0)

	for rows.Next() {
		var messageID int
		var fromID int
		var fromLogin string
		var message string
		var time int

		err := rows.Scan(&messageID, &fromID, &fromLogin, &message, &time)

		if err != nil {
			log.Println(err.Error())
			WriteResponse(w, http.StatusInternalServerError, map[string]string{
				"message": "Internal error",
			})
			return
		}

		m := map[string]interface{}{
			"messageID": messageID,
			"fromID":    fromID,
			"fromLogin": fromLogin,
			"message":   message,
			"time":      time,
		}

		messages = append(messages, m)
	}

	WriteResponse(w, http.StatusOK, messages)
}
