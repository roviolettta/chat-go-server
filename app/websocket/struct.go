package websocket

import (
    "github.com/gorilla/websocket"
)

type SocketEvent struct {
	Event string
    Payload any
}

type Client struct {
    webSocketConnection *websocket.Conn
    send chan SocketEvent
    userID int
}

type Hub struct {
	clients    map[*Client]bool
	register   chan *Client
	unregister chan *Client
}