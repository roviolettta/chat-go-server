package websocket

import (
	"log"
	"time"

	"chat/server/app/libs/db"
)

func UserRegisterEvent(client *Client) {
	HubSoket.clients[client] = true
	handleSocketPayloadEvents(client, SocketEvent{
		Event:   "connect",
		Payload: client.userID,
	})
}

func UserDisconnectEvent(client *Client) {
	_, ok := HubSoket.clients[client]
	if ok {
		delete(HubSoket.clients, client)
		close(client.send)

		handleSocketPayloadEvents(client, SocketEvent{
			Event:   "disconnect",
			Payload: client.userID,
		})
	}
}

func EmitToSpecificClient(Payload SocketEvent, userID int) {
	for client := range HubSoket.clients {
		if client.userID == userID {
			select {
			case client.send <- Payload:
			default:
				close(client.send)
				delete(HubSoket.clients, client)
			}
		}
	}
}

func handleSocketPayloadEvents(client *Client, socketEventPayload SocketEvent) {
	var socketEventResponse SocketEvent
	switch socketEventPayload.Event {
	case "connect":
		log.Println("Connect Event triggered: userID =", client.userID)

		queryString := "UPDATE users SET online = 'online' WHERE id = ?"
		_, err := db.Connection.Exec(queryString, client.userID)

		if err != nil {
			log.Println("Error in handleSocketPayloadEvents: connect:", err.Error())
		}

	case "disconnect":
		log.Println("Disconnect Event triggered: userID =", client.userID)

		queryString := "UPDATE users SET online = 'offline', lastBeen = ? WHERE id = ?"
		_, err := db.Connection.Exec(queryString, time.Now().UTC().UnixMilli(), client.userID)

		if err != nil {
			log.Println("Error in handleSocketPayloadEvents: disconnect:", err.Error())
		}

	case "message":
		log.Println("Message Event triggered: userID =", client.userID)

		selectedUserID := socketEventPayload.Payload.(map[string]interface{})["userID"]
		selectedUserIDConvert, ok := selectedUserID.(float64)

		if ok {

			fromUserID := client.userID
			toUserID := int(selectedUserIDConvert)
			message := socketEventPayload.Payload.(map[string]interface{})["message"]

			socketEventResponse.Event = "message response"
			socketEventResponse.Payload = map[string]interface{}{
				"message": message,
				"from":    fromUserID,
				// индентификатор беседы
			}

			// проверка, может ли данный пользователь отправлять другому сообщение
			queryString := "INSERT INTO direct_messages (from_user_id, to_user_id, message, time) VALUES (?, ?, ?, ?)"
			_, err := db.Connection.Exec(queryString, fromUserID, toUserID, message, time.Now().UTC().UnixMilli())

			if err != nil {
				log.Println("Error in handleSocketPayloadEvents: disconnect:", err.Error())
			} else {
				EmitToSpecificClient(socketEventResponse, toUserID)
			}

		} else {
			log.Println("Error in handleSocketPayloadEvents: message: invalid type")

			socketEventResponse.Event = "error"
			socketEventResponse.Payload = map[string]interface{}{
				"message": "invalid message",
			}

			EmitToSpecificClient(socketEventResponse, client.userID)
		}

	case "friend request":
		log.Println("Friend Request Event triggered: userID =", client.userID)
		// уведомления 'запрос в друзья'
	}

}
