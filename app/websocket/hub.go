package websocket

var HubSoket *Hub

// NewHub will will give an instance of an Hub
func NewHub() {
	HubSoket = &Hub{
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}

	go run()
}

func run() {
	for {
		select {
		case client := <-HubSoket.register:
			UserRegisterEvent(client)

		case client := <-HubSoket.unregister:
			UserDisconnectEvent(client)
		}
	}
}
