package helpers

import (
	"github.com/go-chi/jwtauth"
)

func CreateToken(claims map[string]interface{}) string {
	jwtSecret := GetEnv("JWT_SECRET", "")
	TokenAuth := jwtauth.New("HS256", []byte(jwtSecret), nil)

	_, tokenString, _ := TokenAuth.Encode(claims)
	return tokenString
}
