package helpers

import (
	"os"
)

func GetEnv(key, fallback string) string {
	res := fallback

	if fromEnv := os.Getenv(key); fromEnv != "" {
		res = fromEnv
	}

	return res
}
