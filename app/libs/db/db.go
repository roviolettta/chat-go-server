package db

import (
	"database/sql"
	"time"

	"chat/server/app/libs/helpers"
	_ "github.com/go-sql-driver/mysql"
)

var Connection *sql.DB

func CreatePool() {
	url := helpers.GetEnv("DB_URL", "")
	password := helpers.GetEnv("DB_PASSWORD", "")

	db, err := sql.Open("mysql", "root:"+password+"@tcp("+url+")/chat")

	if err != nil {
		panic(err)
	}
	// See "Important settings" section.
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	Connection = db
}
