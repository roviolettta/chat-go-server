package redis

import (
	"chat/server/app/libs/helpers"
	"github.com/redis/go-redis/v9"

	"context"
	"log"
)

var Client *redis.Client

func CreateClient() {

	url := helpers.GetEnv("REDIS_URL", "")
	password := helpers.GetEnv("REDIS_PASSWORD", "")

	Client = redis.NewClient(&redis.Options{
		Addr:     url,
		Password: password,
		DB:       0, // use default DB
	})

	ctx := context.Background()
	err := Client.Ping(ctx).Err()
	if err != nil {
		log.Panicln("Redis is not working:", err)
		return
	}
}
