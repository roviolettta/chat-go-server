# syntax=docker/dockerfile:1

FROM golang:1.19

# Set destination for COPY
WORKDIR /app

# Download Go modules
COPY go.mod go.sum ./
RUN go mod download

COPY . .

EXPOSE 3000

ENV SERVER_ENV=prod

# Run
CMD ["go", "run", "."]